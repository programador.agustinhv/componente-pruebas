# COMPONENTE

Este proyecto es una prueba para componentes, su principal funcion en integrar y ofrecer nuevas funcionalidades dentro de una pagina o sistema web.

## INSTALACION

El componente esta pre-compilado no es necesario instalar paquetes, pero si se debe de agregar librerias dentro del proyecto donde se desea utilizar el componente.

Primero se debe de incluir las librerias dentro de la etiqueta head.

El css contiene estilos especificos para el componente y trata de no alterar otros elementos de la pagina anfitrion.

>RUTA CSS: /css/simedis.css

```html
<link href="css/simedis.css" rel="stylesheet">
```

El js es el componente en si; crea y ejecuta el componente

>RUTA JS: /js/simedis.js

```html
<script src="js/simedis.js" defer></script>
```
## UTILIZAR EL COMPONENTE

Una vez que se halla definido el js y css, el componente esta disponible; para utilizarlo se debe de seguir la siguiente estructura y colocarlo dentro del body:


```html
<div id="app-ag">    
  <visor-simedis :props="{alto:320,paciente:2902141,tipo:0,titulo:'NOTAS MEDICAS',texto:'ESTO ES UNA PROPIEDAD QUE SE PASA AL COMPONENTE'}"></visor-simedis>
</div>
```
Es posible controlar visualmente el componente si se establecen estilos dentro del div con el id 'app-ag', esto en caso de que se necesite, tambien acepta clases css.

Por otro lado al componente se le pueden pasar propiedades; y en este caso recibe un json con 5 atributos:

>alto: sirve para establecer un height al componente, como minimo es recomendable establecer 200

>paciente: Coloca el numero en un badge junto a un boton

>tipo: funciona para definir si es derechohabiente(1) o causahabiente(0)

>titulo: Establece un titulo dentro del componente

>texto: El componente tiene un alert y rellena este elemento


## VISTA DEL COMPONENTE
<p align="center"><img src="./Readme/componente.png"></p>